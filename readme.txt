Scilab Scipde Toolbox

Purpose
-------

Scipde is a Scilab toolbox for 1D Partial Differential Equations. 
Currently, scipde solves the heat equation in 1D. 

Features
--------

 * scipde_heat1Dsolve � Solve a 1D diffusion equation
 * scipde_heat1Dsteady � Stationnary state of a 1D diffusion equation

Licence
-------

This toolbox is released under the terms of the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

TODO
----

Integrate a 1D hyperbolic equation solver, e.g.

http://perso.centrale-marseille.fr/~gchiavassa/visible/3A/Hyperbolique/NonLineaire

or from http://wiki.scilab.org/Finite%20Volumes%20in%20Scilab

Authors
-------

 * Copyright (C) 2012 - Michael Baudin
 * Copyright (C) 2008 - Fr�d�ric Legrand

