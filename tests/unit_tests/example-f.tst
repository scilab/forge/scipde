// ====================================================================
// Copyright (C) 2012 - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

N=100;
dx=1/(N-1);
X=linspace(0,1,N)';
S=zeros(N,1);
coef=[[1,1]];
BCtype = ["neumann","dirichlet"];
BCval = [0,0];
U=cos(%pi*X/2)+cos(3*%pi*X/2)+cos(5*%pi*X/2);
//
dt = 0.001;
[U,t]=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,U,dt,[0,0.01]);
E = exp(-%pi^2*t/4)*cos(%pi*X/2)+exp(-9*%pi^2*t/4)*cos(3*%pi*X/2)+..
exp(-25*%pi^2*t/4)*cos(5*%pi*X/2);
atol=1.e-1;
assert_checkalmostequal(U,E,[],atol);
