// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008 - Frédéric Legrand
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function h=scipde_heat1Dinit(N,BCtype,BCval,coef,S,dt)
    // Initialize a 1D diffusion equation
    //
    // Calling Sequence
    // h=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,dt)
    //
    // Parameters
    // N : 1-by-1 matrix of doubles, integer value, number of space points
    // BCtype : 2-by-1 matrix of strings, type of the boundary condition at x=0 and x=1, "neumann" or "dirichlet"
    // BCval : 2-by-1 matrix of doubles, value of the boundary condition at x=0 and x=1
    // coef : a k-by-2 matrix of doubles, the diffusion coefficients with format [[x1,D1];[x2,D2],...[xk,Dk]], where xk=1.
    // S : N-by-1 matrix of doubles, the sources
    // dt : 1-by-1 matrix of doubles, the time step
    // h : a data structure storing the initialization step.
    //
    // Description
    // Prepare the data for a 1D diffusion equation.
	// This is an internal function, which, in general, 
	// should not be used directly by users.
	//
    // Examples
    // N=100;   
    // X=linspace(0,1,N)';
    // U=zeros(N,1);
    // S=zeros(N,1);
    // coef=[[1,1]];
    // BCtype = ["dirichlet","dirichlet"]
    // BCval = [1,0];
	// dt = 0.0001;
    // h=scipde_heat1Dinit(N,BCtype,BCval,coef,S,dt);
    //
    // Bibliography
    // "Informatique Appliquée aux Sciences Physiques", Frédéric Legrand, http://www.f-legrand.fr/scidoc/docopera/numerique/euler/euler/euler.xml
    // 
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2008 - Frédéric Legrand

    fname="scipde_heat1Dinit"

    [lhs, rhs] = argn()
    apifun_checkrhs (fname, rhs , 6 )
    apifun_checklhs (fname, lhs , 0 : 4 )
    //
    apifun_checktype (fname, N , "N" , 1 , "constant" )
    apifun_checktype (fname, BCtype , "BCtype" , 2 , "string" )
    apifun_checktype (fname, BCval , "BCval" , 3 , "constant" )
    apifun_checktype (fname, coef , "coef" , 4 , "constant" )
    apifun_checktype (fname, S , "S" , 5 , "constant" )
    apifun_checktype (fname, dt , "dt" , 6 , "constant" )
    //
    type0 = BCtype(1)
    type1 = BCtype(2)
    lim0 = BCval(1)
    lim1 = BCval(2)
    dx=1/(N-1)
    A=spzeros(N,N)
    B=spzeros(N,N)
    C=sparse(S*dt)
    a=dt/dx^2
    nd=size(coef)
    nd=nd(1)
    // D : diffusion coefficients
    D = zeros(N) 
    j1=1
    for k=1:nd
        j2 = int(coef(k,1)*N)
        d = coef(k,2)
        for j=j1:j2
            D(j)=d
        end
        j1 = j2
    end
    for j=2:N-1
        // Cranck-Nicholson
        A(j,j-1)=-a*D(j)/2;
        B(j,j-1)=a*D(j)/2;
        A(j,j)=1+a*D(j); 
        B(j,j)=1-a*D(j);
        A(j,j+1)=-a*D(j)/2; 
        B(j,j+1)=a*D(j)/2;
    end;
    if type0=="dirichlet" then
        A(1,1)=1;
        A(1,2)=0;
        C(1)=lim0;
    elseif type0=="neumann" then
        A(1,1)=-1;
        A(1,2)=1;
        C(1)=dx*lim0;
    else
        mprintf("%s: Unknown limit condition at x0: ""%s""\n",fname,type0)
    end
    if type1=="dirichlet" then
        A(N,N-1)=0;
        A(N,N)=1;
        C(N)=lim1;
    elseif type1=="neumann" then
        A(N,N)=1;
        A(N,N-1)=-1;
        C(N)=dx*lim1;
    else
        mprintf("%s: Unknown limit condition at x1: ""%s""\n",fname,type1)
    end
    for k=1:nd-1
        // Limit between two diffusion coefficients
        j = int(coef(k,1)*N)-1
        A(j,j-1)=-D(j); 
        A(j,j)=D(j)+D(j+1); 
        A(j,j+1)=-D(j+1);
        B(j,j-1)=0; 
        B(j,j)=0; 
        B(j,j+1)=0; 
        C(j)=0;
    end
    // Store the results
    h=struct()
    h.A = A
    h.B = B
    h.C = C
endfunction
