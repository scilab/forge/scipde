// ====================================================================
// Copyright (C) 2012 - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

N=20;
X=linspace(0,1,N)';
S=X;
coef=[[1,1.]];
BCtype = ["dirichlet","dirichlet"];
BCval = [0,0];
U=zeros(N,1);
E=(X-X.^3)/6;
dt=.001;
[US,niter]=scipde_heat1Dsteady(N,BCtype,BCval,coef,S,U,dt);
atol=1.e-10;
assert_checkalmostequal(US,E,[],atol);
