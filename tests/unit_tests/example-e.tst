// ====================================================================
// Copyright (C) 2012 - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

// <-- JVM NOT MANDATORY -->

N=50;   
X=linspace(0,1,N)';
S=zeros(N,1);
coef=[[1,1]];
BCtype = ["dirichlet","dirichlet"];
BCval = [0,0];
U=sin(%pi*X)+sin(3*%pi*X);
//
dt = 0.0001;
[U,t]=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,U,dt,[0,0.01]);
// Exact:
E = exp(-%pi^2*t)*sin(%pi*X)+exp(-9*%pi^2*t)*sin(3*%pi*X);
atol=1.e-2;
assert_checkalmostequal(U,E,[],atol);


